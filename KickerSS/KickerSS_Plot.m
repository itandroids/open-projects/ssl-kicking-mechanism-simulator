clc
close all

%% Defining RLC

V0 = 180; % volts
C0  = 2700 * 10 ^ ( - 6 ); % faraday
R0 = 10; % ohms

%% Initial poses

q0 = V0 * C0;
i0 = V0 / R0;
x0 = 15 * 10 ^ ( - 3 ); % meters

%% The remaining variables must be defined within simulink matlab function block

%% Simulation and plot
sim("KickerSS.slx");
plot(tout, xANDxp.signals(2).values, '-', 'Color', [0.0 0.0 0.0], 'LineWidth', 2)
grid on;
hold on;
x=[2e-3,4e-3,14e-3];
y=[0.85,1.99,4.84];
sd_vct = [0.2  0.2  0.5];
errorbar(x,y,sd_vct, 'o', 'Color', [0.0 0.0 1.0])
set(gca,'TickLabelInterpreter','latex')
xlabel('Time $[s]$', 'FontSize', 12,'Interpreter','latex')
ylabel('Speed $[m/s]$', 'FontSize', 12,'Interpreter','latex')
legend('Model prediction','Experimental observation', 'FontSize', 12,'Interpreter','latex');